**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

Potřebujete zabezpečit svou provozovnu? Chcete mít přehled, pokud budete někde jinde? A přemýšleli jste už nad kamerovými systémy? Ty vaši provozovnu nejen zabezpečí, ale přinesou vám spoustu dalších výhod! Dotykačka vám navíc umožní je propojit s pokladnou a přiřazovat tak účtenky ke konkrétnímu záznamu! A nejen to, také poskytuje kvalitní kamry s vysokým rozlišením, servis, instalaci i pro začátečníky, volbu délky uložení záznamů nebo možnost vše vzdáleně spravoat. Jistě si uvědomíte, že kamery vidíte dnes na každém rohu. V každé  lepší restauraci, hotelu, obchodě... To proto, že poskytují i důkazy pro  případy  krádeží nebo vysvětlování nejasných situací. Navštivte náš web a vplńte formulář, řekneme vám více!
---


